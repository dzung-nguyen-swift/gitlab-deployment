# GitLab releases brief comparison

## **About**

Normally, most of the source control management system platforms do not include or support CI/CD for users. But not for GitLab. GitLab already developed and integrated GitLab CI to their eco system as an optional feature to use.

In order to define a deployment process with your source code, you just need a *.gitlab-ci.yml* in the root directory of the source.

## **Comparisons**

### **Community edition**

This release can be found on any kind of container repositories for any users to download and deploy. Most trusted and frequently mentioned is [**sameersbn/gitlab**](https://hub.docker.com/r/sameersbn/gitlab/) from Docker Hub.

With a private server, the whole community that we got is our own DevOps (or operation) team, so we might be limited to what our team can handle. But in the end, for a close and private environment, this is the best option.

### **Enterprise edition**

Enterprise release solution is more suitable for *cloud infrastructure*, if we are into this. So we can have all advantages to use and integrate our CI/CD workflow for serverless application architecture.

That's not all, compare to CE, EE is designed for quick long-term and decent suport and audits from the company.

### **Pricing**

The pricing of these releases is introduced clearly at [its site](https://about.gitlab.com/pricing/). 

According to our operation status, I assume we intend to deploy and manage our applications frequently on cloud computing infrastructure. rather than internal use, so EE is the obvious choice. I am going to discuss with details for each pack that both CE and EE offer:

- For *Community* pack (free): it is already wonderful for us to test out many things in the latest verison 10.x, such as: defining our CI/CD flow with Bash scripting, wide community supports. If the basics of this pack is good for us, we may upgrade GitLab manually with the releases from Docker Hub or other container repositories.

- For *Starter* pack ($4/user/month): the only thing valuable to us is *next business day support* feature.  But it seems it does not worth our effort to invest in.

- For *Premium* pack ($19/user/month): some of the features caught my intention: *Disaster Recovery*, *Distributed artifacts globally* (like a global repository) and with shorter *business day support*.

- For *Ultimate* pack ($99/user/month): As the way I see this, this pack may help us with the future security risks from the beginning of the source code and built containers.

## **Opinions**

So, I think first we should follow instructions and deploy the *Community* to get used to it. Then, if things are more serious and worth the investment for long-term plan, the *Premium* pack is a good choice.



