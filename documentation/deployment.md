# Understanding & Deployment document

The purpose of this document is to express writer's own-knowledge and self-experience with GitLab. Any adjustments or confusions are welcomed to be raised and discussed about.

---

## **A brief review of GitLab architecture**

GitLab does not simply contain or require itself to operate. It takes serveral components in order to fully function:

- Nginx: web server; should be used and configured to handle the load balancing and pre-process requests before redirecting to GitLab.

- PostgreSQL: database back-end; to store related data/metadata, users management data...

- Redis: this is not simply used as a caching application; it's purposely used as a communication board for end-users interaction, such as: users management and roles, user tasks, user chat (if we used opt-in Chat app from EE release)...

- Sidekid: service worker; is used to handle sending emails for notifications.

- Unicorn: service worker; is used as a rapid data object layer between the front-end web application with the back-end services. This component bases on Ruby on Rails platform, we might get into (SSH) the master back-end service and query with it (but since version 10.x is released, I suggest that this aciton is not necessary any more).

- Workhorse + Gitaly: persistence service worker; these can be considered to be GitLab's heart core components. Its responsisble is to reach and persist to disks. 

- Runner: service worker; in order to build our application from source, GitLab uses its own CI engine to control any kind of runner to pack or compile source code into artifacts. These are various types of runner for some specific platforms/purposes. For example: runner A is deployed and used for Docker build and packing with latest multi-stage method; runner B is deployed and used for simple compiling and shipping a Java applet to remote servers...

## **Deployment**

Due to self-experience, it is good enough to deploy whole GitLab components as Docker containers onto serveral physical servers (or VMs). 

Questions:

*Why deploy with Docker?*

- Answer: If we chose to use GitLab CE, in the future we may decide to upgrade its latest engine version but without losing our data and sources. So it is easier for us to mount the data volume out and persist directly to server disk.

*Why should deploy onto **serveral** servers? *

- Answer: For some components, it is best for them to run seperately from others, so that they can operate without affecting bad causes while running. For example: we may deploy serveral GitLab runners onto 2 physical servers (or VMs) in order to serve mutliple CI instances. While these running instances are executing their own thing, they may load with high and extreme uses of CPU or memory, so it slows the front-end web application if they are running on the same server.

### **Instructions**

Assume we did choose to deploy whole GitLab with Docker, so it is simple and obvious to go to Docker Hub and follow the instructions, and some of them is well-documented. 

Below is the link to my favorite repository of GitLab CE, but any suggestions are welcomed and discussed with. Any other components such as: Redis, PostgreSQL... in my opinion is any selections should be fine, as long as they are the right required version that work well with this GitLab repository.

- sameersbn/gitlab/: https://github.com/sameersbn/docker-gitlab

Next, we should prepare some GitLab Runners linked to the master GitLab. In my own experience, it is quite more stable and faster if we deploy Runners as a service on a server (or VM) rather than as Docker. The whole process of CI/CD is more efficient while these Runners were installed this way. For more information, please reference [here](https://docs.gitlab.com/runner/executors/#compatibility-chart) first.

- GitLab Runner installation guide: https://docs.gitlab.com/runner/install/


    